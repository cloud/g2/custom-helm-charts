#!/bin/bash
set -e

apt update && apt install -y {{ .Values.apt }}

# S3 client
curl https://dl.min.io/client/mc/release/linux-amd64/mc --create-dirs -o /usr/local/bin/mc
chmod +x /usr/local/bin/mc

printf "\n-----\n[INFO] Initiation completed :|\n\n"
