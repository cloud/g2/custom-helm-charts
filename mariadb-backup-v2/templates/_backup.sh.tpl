#!/bin/bash
set -e

function retention {
  local files=$(mc ls S3/$S3_BUCKET | wc -l)

  if (( $files > $RETENTION_FILES )); then
    local files_diff=$(($files-$RETENTION_FILES))

    echo "[INFO] Found $files file(s), retention policy allows $RETENTION_FILES file(s). Deleting $files_diff oldest file(s)."
    for file in $(mc ls S3/$S3_BUCKET | sort | head -n $files_diff | awk '{print $6}'); do
      echo "[DEBUG] Deleting file: $file"
      mc rm S3/$S3_BUCKET/$file
      # mc rm allows flag --older-than, the retention policy could be time based (substracted from current date)
    done

  else
    echo "[INFO] Found $files file(s), retention policy allows $RETENTION_FILES file(s). No retention applied."
  fi
}

function exclude-databases {
  local prefix="--exclude-databases "
  local excluded_databases=${DB_ARGS#"$prefix"}
  echo "[DEBUG] Excluded databases: $excluded_databases"

  local excluded_databases_regex=$(echo "$excluded_databases" | tr " " "|")
  local exclusions="${excluded_databases_regex}|Warning|Database"
  set +o history
  local databases=$(mysql --host $DB_HOST --port $DB_PORT --user $DB_USER -p${DB_PASSWORD} -s -e "show databases;" 2>&1 | grep -Ev "$exclusions" | tr "\n" " ")
  set -o history
  echo "[DEBUG] Databases being dumped (left after exclusion): $databases"

  set +o history
  mysqldump --host $DB_HOST --port $DB_PORT --user $DB_USER -p${DB_PASSWORD} --databases $databases > /tmp/$S3_BUCKET.sql
  set -o history
}

set +o history
mc alias set S3 $S3_HOSTNAME $S3_ACCESS_KEY $S3_SECRET_KEY
set -o history

if mc ls S3 | grep $S3_BUCKET; then
  echo "[INFO] Target bucket $S3_BUCKET exists."
else
  echo "[INFO] Target bucket $S3_BUCKET doesn't exist and will be created."
  mc mb S3/$S3_BUCKET
fi

# Safety, script will fail here if bucket is unavailable for whatever reason
echo "[DEBUG] Target bucket content:"
mc ls S3/$S3_BUCKET

echo -e "\n[INFO] Dumping database $DB_HOST:$DB_PORT with args: $DB_ARGS"
if [[ $(echo $DB_ARGS | awk '{print $1}') == "--exclude-databases" ]]; then
  exclude-databases
else
  set +o history
  mysqldump --host $DB_HOST --port $DB_PORT --user $DB_USER -p${DB_PASSWORD} $DB_ARGS > /tmp/$S3_BUCKET.sql
  set -o history
fi

echo "[DEBUG] Compressing dump ..."
bzip2 /tmp/$S3_BUCKET.sql

echo "[DEBUG] Uploading to bucket $S3_BUCKET ..."
mc cp /tmp/$S3_BUCKET.sql.bz2 S3/$S3_BUCKET/$S3_BUCKET-$(date +%Y%m%d_%H%M).sql.bz2
rm -f /tmp/$S3_BUCKET.sql.bz2

echo "[INFO] Dumped and compressed database backup uploaded to bucket $S3_BUCKET."
echo "[DEBUG] Target bucket content:"
mc ls S3/$S3_BUCKET

if $RETENTION_ENABLED; then retention; fi
