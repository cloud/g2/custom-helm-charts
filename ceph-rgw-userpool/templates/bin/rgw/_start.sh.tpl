#!/bin/bash

{{/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/}}

set -ex
export LC_ALL=C
: "${CEPH_GET_ADMIN_KEY:=0}"
: "${RGW_NAME:=$(uname -n)}"
: "${RGW_ZONEGROUP:=}"
: "${RGW_ZONE:=}"
: "${ADMIN_KEYRING:=/etc/ceph/${CLUSTER}.client.admin.keyring}"
: "${RGW_KEYRING:=/var/lib/ceph/radosgw/${RGW_NAME}/keyring}"
: "${RGW_DISPATCHED_USER_KEYRING:=/tmp/rgw-user-keyring/keyring}" # ceph user keyring dispatched and exported by rgw-user-dispatcher

if [[ ! -e "/etc/ceph/${CLUSTER}.conf" ]]; then
  echo "ERROR- /etc/ceph/${CLUSTER}.conf must exist; get it from your existing mon"
  exit 1
fi

if [ "${CEPH_GET_ADMIN_KEY}" -eq 1 ]; then
  if [[ ! -e "${ADMIN_KEYRING}" ]]; then
      echo "ERROR- ${ADMIN_KEYRING} must exist; get it from your existing mon"
      exit 1
  fi
fi

# Check to see if our RGW has been initialized
if [ ! -e "${RGW_KEYRING}" ]; then

  mkdir -p "$(dirname "${RGW_KEYRING}")"
  if [ -e "${RGW_DISPATCHED_USER_KEYRING}" ]; then
    cp -f "${RGW_DISPATCHED_USER_KEYRING}" "${RGW_KEYRING}"
    chown ceph. "${RGW_KEYRING}"
    chmod 0600 "${RGW_KEYRING}"
  else
    echo "ERROR: This version of ceph-rgw-userpool requires predefined pool of rgw users present in secret os-ceph-rgw-users"
    echo "       rgw-user-dispatcher init container chooses non-coliding user for each rgw instance."
    echo "       rgw-user-dispatcher did not provide rgw user keyring, see kubectl --context=ca -n openstack logs -l application=ceph -l component=rgw -c ceph-rgw-userpool for more details."
    exit 2
  fi

fi

RGW_USER_NAME="$(cat "${RGW_KEYRING}" | grep -E '^\[' | grep -Eo '[a-zA-Z0-9_\.-]+')"
timeout 10 ceph --cluster "${CLUSTER}" --name "${RGW_USER_NAME}" --keyring "${RGW_KEYRING}" health || exit 1

exec /usr/bin/radosgw \
  --cluster "${CLUSTER}" \
  --setuser "ceph" \
  --setgroup "ceph" \
  -d \
  -n "${RGW_USER_NAME}" \
  -k "${RGW_KEYRING}" \
  --rgw-socket-path="" \
  --rgw-zonegroup="${RGW_ZONEGROUP}" \
  --rgw-zone="${RGW_ZONE}" \
  ${RGW_ARGS}
